jQuery(document).ready(function($) {
    
    $('.tfr-details-link').click(function(){
    	var wrapperId = this.id;
    	var wrapper = $('.' + wrapperId);
    	if (wrapper.is(':hidden'))
    		wrapper.show();
    	else
    		wrapper.hide();
    });
    $('#tfr-result-description-hideall').click(function(){
    	$('.result-table-wrapper').hide();
    });
    $('#tfr-result-description-showall').click(function(){
    	$('.result-table-wrapper').show();
    }); 
    
});