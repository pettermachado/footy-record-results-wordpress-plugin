<?php
/*
	Plugin Name: Footy Record Results
	Plugin URI: http://www.pettereek.se/footy-record-results/
	Description: This plugin file_get_content's over to thefootyrecord.net and fetches the result form the given competition.
	Version: 0.0.1
	Author: Petter Eek
	Author URI: http://www.pettereek.se
	License: GPL2
 
	Copyright 2010 - current  Petter Eek  (email : hello@pettereek.se)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
*/


if ( !is_admin() ) { // instruction to only load if it is not the admin area
   wp_register_script('footyrecord-jquery', 
   site_url() .'/wp-content/plugins/footyrecord-results/footyrecord-results-js.js',
   array('jquery'));
   wp_enqueue_script('footyrecord-jquery');
}

add_shortcode('tfr-results','tfr_result_handler');

function tfr_result_handler( $atts, $content = null, $code ) {
	
	extract( shortcode_atts( array(
		'competition' => false,
		'show_table_header' => false,
		'show_future_games' => false
		), $atts ) );
    
    $mirror_url = 'http://www.thefootyrecord.net/index.php?content1=comp&c='.$competition.'&taboption=3'; // Similar page on tfr.net
    
    if (!$competition) : // If no competition is supplied there's really not much to do.
    	return "TFR results table error: No competition ID supplied.";
    endif;
    
	$matches = tfr_api('match&w=CompetitionId='.$competition.'&o=StartPoint');
	foreach($matches as $match) {
		if ( array_key_exists('Id', $match )) {
			// Set the Unix timestamp
			$match['timestamp'] = strtotime($match['StartPoint']);
			
			$scores_a = array();
			// Get the scoreline if the game has been played
			if ( $match['timestamp'] < time() ) {
				$scoreline = tfr_api('scoreline&w=MatchId='.$match['Id']);
				unset($scoreline[0]);

				// Sort the scores
				foreach ($scoreline as $qtrscore) {
					$scores_a[$qtrscore['SideNbr']][$qtrscore['Quarter']] = $qtrscore;
					
					// And set the total score for convenience
					if ($qtrscore['FullTime'] == '1')
						$scores_a[$qtrscore['SideNbr']]['total'] = $qtrscore['Goals'] * 6 + $qtrscore['Behinds'];
				}
				
				$match['scoreline'] = $scores_a;
			}
			
			// Get the venue
			$ground = tfr_api('ground&w=Id='.$match['GroundId']);
			$match['ground'] = $ground[1];
			
			// Get the team ids
			$sideteams = tfr_api('sideteam&w=MatchId='.$match['Id']);
			
			// Get the teams
			if ( count($sideteams) == 3 ) {
				foreach ( $sideteams as $side ) {
					if ( array_key_exists('TeamId', $side ) ) {
						$team = tfr_api('team&w=Id='.$side['TeamId']);
						$match['team_'.$side['SideNbr']] = $team[1];
					}
				}
			}
			if ($latest == null)
				$latest = $match;
			$matches_a[count($matches_a)] = $match;
		} // if is match
	} // foreach

    ob_start(); // Start buffering
?>
<table id="tfr-fixture-table" cellpadding="0" cellspacing="0">
<?php if ($show_table_header) : ?>
<thead><tr><td>&nbsp;</td><td>&nbsp;</td><td>1</td><td>2</td><td>3</td><td>4</td></tr></thead>
<?php endif; ?>
<tbody>
<?php foreach ($matches_a as $match) { 
	if ($match['timestamp'] < time() || $show_future_games) : ?>
<tr>
	<td class="tfr-match-date"><?php echo date('D jS M', $match['timestamp']); ?></td>
	<td class="tfr-match-time"><?php echo date('H:i', $match['timestamp']); ?></td>
	<?php 
	// See who's the winner
	$home_winner = $match['scoreline']['1']['total'] > $match['scoreline']['2']['total'] ? "winner" : "loser";
	$away_winner = $match['scoreline']['1']['total'] < $match['scoreline']['2']['total'] ? "winner" : "loser";
	?>
	<td class="tfr-match-team" id="<?php echo $match['Id']; ?>">
		<span class="tfr-match-home <?php echo $home_winner; ?>"><?php echo $match['team_1']['LocalName']; ?> (<?php echo $match['scoreline']['1']['total'];?>)</span> <span class="tfr-match-team-versus">vs.</span> 
		<span class="tfr-match-away <?php echo $away_winner; ?>"><?php echo $match['team_2']['LocalName'];  ?> (<?php echo $match['scoreline']['2']['total'];?>)</span></td>
	<td class="tfr-match-ground"><?php echo $match['ground']['ShortName'];  ?></td>
	<td class="tfr-match-round">Rd <?php echo $match['RoundId']; ?></td>
	<!-- <?php if($match['RoundId'] > 9) : print_r($match); endif; ?> -->
	<td class="tfr-match-jquery"><?php if ( array_key_exists( 'scoreline', $match )) : ?><span id="tfr-details-<?php echo $match['Id']; ?>" class="linkstyle tfr-details-link">Details</span><?php endif; ?></td>
</tr>
<?php if ( array_key_exists( 'scoreline', $match )) : ?>
<tr class="tfr-result-row" id="tfr-result-row-<?php echo $match['Id']; ?>">
	<td colspan="6" class="result-cell">
	<div class="result-table-wrapper tfr-details-<?php echo $match['Id']; ?>">
	<table class="result-table" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<td>&nbsp;</td>
				<td colspan="3">1st</td>
				<td colspan="3">2nd</td>
				<td colspan="3">3rd</td>
				<td colspan="3">Full</td>
			</tr>
		</thead>
		<tbody>
			<?php for ($i = 1; $i <= 2; $i++) { ?>
			<tr>
				<td class="tfr-result-team"><?php echo $match['team_' . $i]['LocalName']; ?></td>
			<?php for ($j = 1; $j <= 4; $j++) {
				$goal = $match['scoreline'][$i][$j]['Goals'];
				$behind = $match['scoreline'][$i][$j]['Behinds'];
				$total = $goal * 6 + $behind;
				$fulltime = ''; 
				if ($match['scoreline'][$i][$j]['FullTime'] == '1')
					$fulltime = 'tfr-result-qtr-fulltime';
				?>
				<td class="tfr-result-qtr-goal <?php echo $fulltime; ?>"><?php echo $goal; ?></td>
				<td class="tfr-result-qtr-behind <?php echo $fulltime; ?>"><?php echo $behind; ?></td>
				<td class="tfr-result-qtr-total <?php echo $fulltime; ?>"><?php echo $total; ?></td>						
				<?php } // j	?>
			</tr>
			<?php } // i ?>
		</tbody>
	</table>
	</div>
	</td>
</tr>
<?php endif; ?>
<?php endif; // if past game
} // foreach ?>
</tbody>
</table>
<p class="tfr-result-controls"><span id="tfr-result-description-showall" class="linkstyle">Show all</span> | <span id="tfr-result-description-hideall" class="linkstyle">Hide all</span></p>
<p class="tfr-credit">Data from <a href="<?php echo $mirror_url; ?>">The Footy Record</a></p>

	<?php $content = ob_get_contents(); // Copy the buffer
    ob_end_clean(); // Empty the buffer
	return $content;
}

function tfr_api($p) {
	$query = 'http://www.thefootyrecord.net/index.php?content1=api&m=sql&t=' . $p;
	// TODO: Store this to db and update every hour or so.
	$contents = @file_get_contents($query); // The @ makes this fail silently
	return json_decode($contents, true);
}

?>